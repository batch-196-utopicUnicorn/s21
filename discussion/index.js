console.log("Hello World");

//Arrays
//Arrays are used to store multiple related data/values in a single variable.
//It is created/declared using [] brackets also know as "Array Literals"

let hobbies = ["Play video games", "Combat sports", "Play with animals", "Studying", "Watching anime or movies"];

//Arrays make it easy to manage, manipulate a set of data. Arrays have different methods/functions that allows us to manage our array.
//Methods are functions associated with an object.
//Because Arrays are actially a special type of object.

console.log(typeof hobbies);

let grades = [78, 77, 76, 81, 83];
const planets = ["Mercury", "Venus", "Mars", "Earth"];

//Arrays as a best practice contains values of the same type.

let arraySample = ["Saitama", "One Punch Man", 25000, true];

//However, since there are no problems to creating arrays like this, you may encounter exceptions to this rule in the future and in fact in other JS libraries or frameworks

//Arrays as a collection of data, has methods to manipulate and manage the array. Having values with different data types might interfere or conflict with the methods of an array.

const dailyRoutine = ["30 Push ups", "Eat fruits and vegetables", "Check up on my grandparents", "Prepare for the next day", "Sleep not less than 6 hours"]

const capitalCities = ["Manila", "Hong Kong", "Paris", "Tokyo", "Berlin"]

console.log(dailyRoutine);
console.log(capitalCities);

//Each item in an array is called an element.

//Array as a collection of data, as a convention, its name is usually plural.

//We can also add the values of variables as element in an array.

let username1 = "figther_smith1";
let username2 = "goerge5600";
let username3 = "white_night";

let guildMembers = [username1, username2, username3];

console.log(guildMembers);

//.length property
//The .length property of an array tells about the number of elements in the array.

//It can actually also be set and manipulated.
//The .length property of an array is a number type.

console.log(dailyRoutine.length);
console.log(capitalCities.length);

//In fact, even the strings have a .length property, which tells us the number of characters in a string.

//strings are able to use some array methods and properties.

let fullName = "Randy Orton";
console.log(fullName.length);

//We can manipulate the .length property of an array. Being that .length property is a number that tells the total numbers of elements in an array, we can also delete the last item in an array by manipulating the .length property.

dailyRoutine.length = dailyRoutine.length-1;
console.log(dailyRoutine.length);
console.log(dailyRoutine);

//We cannot yet delete a specific item in our array, however in the session, we can learn of an array method which will allow us to do so.
//We can do it now, manually, but that would require us to create our own algorithm to solve that problem.

//We could also decrement the .length property of an array. It will produce the same result, that it will delete the last item in the array.




function deleteArr(arrayy){
	arrayy.length--;
}
deleteArr(capitalCities);
console.log(capitalCities);

//Can we do the same thing to a string?
//No

fullName.length = fullName.length-1;
console.log(fullName)

//If we can shorten the array by updating the length property, can we also lengthen or add using the same trick?

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

//Accessing the elemts of an array
//Accessing the array elements is one of the more common task we do with an array.
//This can be done through the use of array indices.
//Array elements are ordered according to index.

console.log(capitalCities);

//If we want to access a particular item in the array, we can do so with array indices. Each item are ordered according to their index. We can locate items in an array via their index. To be able to access an item in an array, first identify the name of the array, then add [] square brackets and then add the index number of your item.

//syntax: arrayNamep[index]

//note: index are number types.

console.log(capitalCities[0]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);//Shaq
console.log(lakersLegends[3]);//Magic

//We can also save/store a particular array element in a variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

//We can also update/reassign the array element using their index..

lakersLegends[2] = "Pau Gasol";
console.log(lakersLegends);


let favoriteFoods = [
	
	"Tonkatsu",
	"Adobo",
	"Hamburger",
	"Sinigang",
	"Pizza"
]
favoriteFoods[3] = "Paella";
favoriteFoods[4] = "Pizza with pineapple";
console.log(favoriteFoods)


//What if we do now know the toatl number of items in the array?
//what if the array is constantly being added into?

//we could consistently access the last item in our array by accessing adding the .length property value minus 1 as the index.

console.log(favoriteFoods[favoriteFoods.length-1]);
console.log(favoriteFoods.pop());

let bullsLegend = ["Jordan", "Pippen", "Rodman" , "Rose", "Scalabrine", "Kukoc", "Lavine"];
console.log(bullsLegend[bullsLegend.length-1]);

//Add items in an array
//Using our indices we can also add items in our array.

let newArr = [];
console.log(newArr);
console.log(newArr[0]);

//Loop over and display all items in the newArr array:

for(let index = 0; index < newArr.length; index ++){

	console.log(newArr[index])
}

let numArr = [5, 12, 30, 46, 40];


//check each item in the array if they are divisible by 5 or not.
for(let index = 0; index < numArr.length; index ++){

	//loop over every item in the numArr array. The current iteration or the current loop number will be used as index.


	//first loop = index = 0 = numArr[index] = numArr[0];
	//2nd loop = index = 1 numArr[index] = numArr[1]
	//...
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}else{
		console.log(numArr[index] + " is not divisible by 5")
	}
}


//Multidimentional Arrays

//Multidimentional arrays are arrays that contain other arrays.

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard[7][0])
console.log(chessBoard[5][7])